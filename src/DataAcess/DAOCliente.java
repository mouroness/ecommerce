/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAcess;

import Telas.Cliente;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Gustavo Mourão < grm  at grm.com >
 */
public class DAOCliente {
    
    public void insert(Cliente cc) throws SQLException, ClassNotFoundException, Exception {

        //Inserir dados do cliente no banco de dados 
        
        PreparedStatement ps = ConnectionFactory.getConnection().prepareStatement("insert into sc_ecommerce.cliente"
                + " (cpf, nome, rua, bairro, cidade, estado, numero, cep) VALUES (?,?,?,?,?,?,?,?);");

        ps.setString(1, cc.getCpf());
        ps.setString(2, cc.getNome());
        ps.setString(3, cc.getRua());
        ps.setString(4, cc.getBairro());
        ps.setString(5, cc.getCidade());
        ps.setString(6, cc.getEstado());
        ps.setInt(7, cc.getNumero());
        ps.setLong(8, cc. getCep());
        
        int row = ps.executeUpdate();
        if (row == 0) {
            throw new Exception("Inserção não realizada");
        }
    }
}
